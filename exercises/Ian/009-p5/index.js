
const CELL_SIZE =64
const speed = 3;

const WIDTH=10
const HIGH = 10
let w=0;
let h=0;
let pos_x = 0;
let pos_y = 0;
let cont=0;


function setup() {
  // put setup code here
  w = WIDTH * CELL_SIZE;
  h = HIGH * CELL_SIZE;
  const canvas = createCanvas(w, h);
  canvas.parent('#canvasHolder');
}

function draw() {
    background(0);    
    
    if(cont%2==0)
      fill('#2364AA');
    else  
      fill(50);  

    rect(pos_x,pos_y,CELL_SIZE,CELL_SIZE); 
     
    // pos_x = pos_x +  speed;
    pos_y = pos_y +  speed;
    // if(pos_x >= w )
    //    pos_x = 0;
    if(pos_y >= h )
      pos_y = 0; 
      
      
    cont++;   
}

