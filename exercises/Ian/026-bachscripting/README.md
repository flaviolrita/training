


# exercises
- 01 : imprimir por consola 'hello world '+[username] 
- 02 : sumar dos argumentos que se pase al acchivo
- 03 : pasar un numero como argumento y que retorne si es par o impar
- 04 : determinar la comparacion de dos numeros


# permits
```
chmod +x ./01.sh
chmod +x ./02.sh
chmod +x ./03.sh
chmod +x ./04.sh
chmod +x ./05.sh
chmod +x ./06.sh
chmod +x ./07.sh
chmod +x ./08.sh
chmod +x ./09.sh
chmod +x ./10.sh
```


# references
- [tutorial](https://likegeeks.com/es/script-de-bash-tutorial/)
- [operators](https://tldp.org/LDP/abs/html/comparison-ops.html)