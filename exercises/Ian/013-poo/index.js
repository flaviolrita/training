

class Animal
{
   constructor(name){
       this._name = name;  
   }

   get name(){return this._name;}
   get blood(){return null;}

   sound(){
       return null;
   }
}

class  Mamifero extends Animal
{
    get blood(){return "hot";}
}
class  Reptil extends Animal
{
    get blood(){return "cold";}
}


class  Iguana extends Reptil
{
    sound(){
        return "";
    }
}

class  Gato extends Mamifero
{
    sound(){
        return "miau";
    }
}
class  Perro extends Mamifero
{
    sound(){
        return "guau";
    }
}


let animales = [];

animales.push(new Iguana("claudia"));
animales.push(new Perro("pampa"));
animales.push(new Perro("zara"));
animales.push(new Gato("manolo"));

for(let i=0;i<animales.length;i++){
    let animal = animales[i];
    console.log(animal.name+" es "+(animal.constructor.name)+" dice "+animal.sound()+" y tienen sangre "+animal.blood);
}


// Ejecicio: crear la clase base Vehiculo, y las clases auto, moto, camioneta y lancha
//  Propiedades
//      patente
//      capacidad (asientos)
//      largo
//      ancho
//      alto
//      peso 
//  Metodos:
//      volumen    

//  las clases auto, moto , camioneta y lancha
//  Propiedades
//     medio (sobre que se deplaza)
//     ruedas


// Hacer un array de vehiculos  
//     cargar distintos veiculos
//     recorerr el array y mostrar el texto por ejejmplo "El vehiculo XXS234 es Auto, tienen x asientos y tiene un volumen de a 120 mts3 "

