const grid = []
const CELL_SIZE =35

const WIDTH=10
const HIGH=20


function setup() {
  const w = WIDTH * CELL_SIZE;
  const h = HIGH * CELL_SIZE;
  const canvas = createCanvas(w, h);
  canvas.parent('#canvasHolder');

  for(let y =0;y < HIGH;y++){
    let row = [];
    for(let x =0;x < WIDTH;x++){
      row.push(new Cell(x,y));
    }
    grid.push(row);
  }

}

function draw() {
    background(0);

    for(let y =0;y < HIGH; y++){
      for(let x =0;x < WIDTH; x++){
        grid[y][x].draw(CELL_SIZE);
      }
    }
}

