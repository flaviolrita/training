class Cell{
    constructor(x,y){
      this.x = x;
      this.y = y;
  
      this.visible = false;
    }
  
    draw(size){
      let i = this.x * size;
      let j = this.y * size;
  
      fill('#2364AA');

      noStroke();
      rect(i,j,size,size);

      stroke(0);
      strokeWeight(2);
      noFill();

     line(i,j,i,j+size);
     line(i,j,i+size,j);
     line(i+size,j,i+size,j+size);
     line(i,j+size,i+size,j+size);
    }   
  
  
  }