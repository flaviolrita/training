const mysql = require('mysql2');


class MysqlProvider
{
    constructor(connectionInfo){
        this.connectionInfo = connectionInfo;
    }
    async execute(scripts){
        let results= [];
        const pool= mysql.createPool(this.connectionInfo);
        const promisePool = pool.promise();
        for(let i = 0;i<scripts.length;i++){
            let script = scripts[i]; 
            let result = await promisePool.query(script);
            results.push(result);
        }
        return results;
    }
}



(async () => { 
    try {        
        let cnx = {host:"localhost",port:3306,user:"root",password:"mariadb"};
        let sqlProvider = new MysqlProvider(cnx);
        
        let data = {database: "mydb",user:"mydb",password:"mydb"};

        let scripts = []; 
        scripts.push("CREATE DATABASE "+data.database+";");
        scripts.push("CREATE USER '"+data.user+"'@'%' IDENTIFIED BY '"+data.password+"';"); 
        scripts.push("GRANT ALL PRIVILEGES ON "+data.database+".* TO '"+data.user+"'@'%';");
        scripts.push("FLUSH PRIVILEGES;"); 
          
        let results = await sqlProvider.execute(scripts); 
        results.forEach(p => console.log(p));


    }
    catch (error) {     
        console.error(error);  
    }    
})();