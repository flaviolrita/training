# Exercise 2
## Avance
* Se creo una clase Shape con las propiedades y metodos asociados al Shape
* Se modifico la clase Piece para que reciba un shape como parametro
* Se definio por configuracion los distintos tipos de shapes
* Se definio por configuracion diferentes strategias para obtener la proxima pieza
* Se creo una clase por cada tipo de estrategia
* Se creo la clase Game     
## Tarea

* con drawIO crear un diagrama que contenga todas las clases.
* recorer el codigo y ver si se puede reemplazar for por ForEach, o donde se pueda reemplazar codigo por funciones lambda.
  - [reference](https://lenguajejs.com/javascript/caracteristicas/array-functions/)
* Leer sobre TypeScript
  - [reference](https://softwarecrafters.io/typescript/typescript-tutorial-javascript-introduccion) 
  - [enable tsc en windows](https://www.youtube.com/watch?v=hMtmLTsxdAM) 
  - [curso](https://desarrolloweb.com/manuales/manual-typescript.html)