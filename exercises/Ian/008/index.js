
// CRUD
// https://www.ionos.es/digitalguide/paginas-web/desarrollo-web/crud-las-principales-operaciones-de-bases-de-datos/

const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const crud = require('./lib.js');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use("/", router);



app.get('/user', function (req, res) {
  let users = crud.listUsers();  
  res.send(users);
});
app.get('/user/:id', function (req, res) {
    let user = crud.getUser(req.params.id);  
    res.send(user);
}); 
app.post('/user', function (req, res) {     
    let user= crud.addUser(req.body); 
    res.send(user);
});
app.put('/user', function (req, res) {     
    let updated= crud.updateUser(req.body); 
    res.send(updated);
});
app.delete('/user/:id', function (req, res) {     
    let deleted= crud.deleteUser(req.params.id); 
    res.send(deleted);
});

app.listen(3000);