
const fs = require('fs');
const path = require('path');


let fullpath  =  path.join(__dirname,'data.json');
let str = fs.readFileSync(fullpath,'utf-8');
let data=  JSON.parse(str);


//console.log(data.users);




let result;

//function map 
//https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/map
function map(list){
    let result = [];     
    for(let i=0;i<list.length;i++){
        let user =list[i];
        result.push( user.firstName+' '+user.lastName);
    }
    return result;
}
//result = data.users.map(function(user){ return user.firstName+' '+user.lastName; });
result = data.users.map(p => p.firstName+' '+p.lastName);

//------------- Filter ----------------------------------------------------------------------------- 

//https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/filter
//result =  data.users.filter(function(user){return user.firstName && user.firstName.startsWith('d');});
result =  data.users.filter(p=> p.firstName &&  p.firstName.startsWith('d'));

result =  data.users.filter(p=> p.firstName &&  p.firstName.startsWith('d'))
                    .map(p =>  p.firstName+' '+p.lastName)
                    .sort()
                    .reverse();

//------------- Find ----------------------------------------------------------------------------- 
result =  data.users.find(p=> p.firstName &&  p.firstName.startsWith('d'));

//------------- ForEach ----------------------------------------------------------------------------- 
let total=0;
data.users.forEach(p => total+= (p.age || 0)  );

//------------- IndexOf ----------------------------------------------------------------------------- 
result =  data.users.findIndex(p=> p.userId == 8);

console.log(result);