






# references
[DML DDL](https://todopostgresql.com/diferencias-entre-ddl-dml-y-dcl/)
[DER] (https://www.lucidchart.com/pages/es/como-hacer-un-diagrama-entidad-relacion)




# data
- customers
  - id
  - dni
  - name
  - birthdate
  - billingAdressId
  - deliveryAddressId

- address
  - id
  - street
  - number
  - zipcode
  - city
  - state
  - region
  - country
  
- invoices
  - id
  - date
  - customerId
  - number
  - taxRate
  
- invoiceItems
  - id
  - invoiceId
  - description
  - qty
  - unitPrice

# DDL
## CREATE DATABASE AND USER
```
CREATE DATABASE test;
CREATE USER 'test'@'%' IDENTIFIED BY 'test';
GRANT ALL PRIVILEGES ON test.* TO 'test'@'%';
FLUSH PRIVILEGES;
```

### create table address
```
CREATE TABLE address (
  id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  street varchar(20)  NOT NULL DEFAULT '',
  number varchar(10) NOT NULL DEFAULT '',
  zipcode varchar(20) NOT NULL DEFAULT '',
  city varchar(80) NOT NULL DEFAULT '',
  state varchar(80) NOT NULL DEFAULT '',
  region varchar(80) NOT NULL DEFAULT '',
  country  varchar(80) NOT NULL DEFAULT '',
  PRIMARY KEY (id)
);
```

### create table Customers
```
CREATE TABLE customer (
  id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  dni varchar(20)  NOT NULL DEFAULT '',
  name varchar(80) NOT NULL DEFAULT '',
  birthdate datetime NULL ,
  billingAdressId bigint(20) unsigned NOT NULL,
  deliveryAddressId bigint(20) unsigned NOT NULL,   
  PRIMARY KEY (id),
  FOREIGN KEY (billingAdressId) REFERENCES address(id)ON DELETE CASCADE,
  FOREIGN KEY (deliveryAddressId) REFERENCES address(id)ON DELETE CASCADE
);

``` 
### create table invoices
//TODO:

### create table invoiceItems
//TODO:

## DROP DATABASE
```
DROP DATABASE test;
```


# Task

- crear grafico de DER
- crear y ejecutar script para crear tablas de invoice e invoiceItems