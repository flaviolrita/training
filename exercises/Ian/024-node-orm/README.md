

# references
## orms
### sequielize
- [npm](https://www.npmjs.com/package/sequelize)
- [documentation](https://sequelize.org/master/)
- [blog](https://ed.team/blog/agiliza-tu-desarrollo-en-nodejs-con-el-orm-sequelize) 
#### examples
- [node-js-express-sequelize-mysq](https://bezkoder.com/node-js-express-sequelize-mysql/)
- [example](https://github.com/sequelize/express-example)
- [sequelize-express-example](https://github.com/shaishab/sequelize-express-example)
### prisma
- [npm](https://www.npmjs.com/package/@prisma/client)
- [tutorial](https://www.prisma.io/docs/getting-started/quickstart-typescript)
- [exercise](https://www.prisma.io/docs/getting-started/setup-prisma/add-to-existing-project-typescript-postgres)
### typeorm
- [npm](https://www.npmjs.com/package/typeorm)
- [typeorm + nestjs](https://www.npmjs.com/package/@nestjs/typeorm)
- [tutorial](https://docs.nestjs.com/techniques/database)
### objection
- [npm](https://www.npmjs.com/package/objection)
- [tutorial](https://vincit.github.io/objection.js/api/query-builder/)

## postgress REST
- [resp api](https://postgrest.org/en/latest/)
- [GraphQL API](https://hasura.io/)
  