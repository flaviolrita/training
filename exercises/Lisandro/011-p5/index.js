
const PIXEL_SIZE =64
const speed = 3;

const WIDTH=10
const HIGH = 10
let w=0;
let h=0;
let pos_x = 0;
let pos_y = 0;
let cont=0;
let cell_count=0;
let incremento_y=0;
let incremento_x=0; 



function setup() {
  // put setup code here
  w = WIDTH * PIXEL_SIZE;
  h = HIGH * PIXEL_SIZE;
  cell_count  = WIDTH * HIGH;
  const canvas = createCanvas(w, h);
  canvas.parent('#canvasHolder');

  incremento_y = h/10;
  incremento_x = h/1000;

 
}

function draw() {
    background(0);    
    //fill('#2364AA');
    stroke(153);

    

    for(let y=0;y<h; y+=incremento_y ){
      line(0, y, w, y);
    }  

    for(let x=w;x<w; x+=incremento_x){
      line(x, 0, x, h);
    }
    
   

    
    //line(w/2, 0, w/2, h);

    // for(let x=(w/2)*-1;x<w/2;x++){      
    //   let y = Math.sqrt(x)+10;// x+10;
     

    //   let coord =  getCoordinate(x,y);
    //   point(coord.x,coord.y);
    // }
  }

  function getCoordinate(x,y){
    return {x: x+(w/2),y:(h/2)-y };
  }
    

  //Ejercicio 1 : dibujar 10 lineas  horizontales (usando for)
  //Ejercicio 2 : dibujar 10 lineas  horizontales y 10 lineas verticales  (usando for)
