


class Vehiculo
{
    constructor(name){
        this._name = name;  
    }

    get patente(){return null;}
    get capacidad(){return null;}
    get largo(){return null;}
    get ancho(){return null;}
    get alto(){return null;}
    get peso(){return null;}

    volumen(){
        return null;
    }
}

class auto extends Vehiculo
{
    patente(){
        return 'XFJ342';
    }
    capacidad(){
        return '4';
    }
    largo(){
        return '4 metros';
    }
    ancho(){
        return '1,5 metros';
    }
    alto(){
        return '1,5 metros';
    }
    peso(){
        return '900 kg';
    }
    get ruedas(){return '4';}

    get medio(){return 'tierra';}

    volumen(){
        return '150 metros^3';
    }
}

class moto extends Vehiculo
{
    patente(){
        return 'DJG467';
    }
    capacidad(){
        return '2';
    }
    largo(){
        return '2 metros';
    }
    ancho(){
        return '0,7 metros';
    }
    alto(){
        return '1 metro';
    }
    peso(){
        return '150 kg';
    }
    get ruedas(){return '2';}

    get medio(){return 'tierra';}

    volumen(){
        return '50 metros^3';
    }
}
class camioneta extends Vehiculo
{
    patente(){
        return 'DLX487';
    }
    capacidad(){
        return '4';
    }
    largo(){
        return '4,5 metros';
    }
    ancho(){
        return '1,5 metros';
    }
    alto(){
        return '2 metros';
    }
    peso(){
        return '1500 kg';
    }
    get ruedas(){return '4';}

    get medio(){return 'tierra';}

    volumen(){
        return '200 metros^3';
    }
}
class lancha extends Vehiculo
{
    patente(){
        return 'LKK228';
    }
    capacidad(){
        return '6';
    }
    largo(){
        return '6 metros';
    }   
    ancho(){
        return '2 metros';
    }
    alto(){
        return '1 metro';
    }
    peso(){
        return '2000 kg';
    }
    get ruedas(){return '0';}

    get medio(){return 'agua';}

    volumen(){
        return '220 metros^3';
    }
}

let vehiculos = [];

vehiculos.push(new auto("etios"));
vehiculos.push(new moto("r6"));
vehiculos.push(new camioneta("hilux"));
vehiculos.push(new lancha("traker 520"));

for(let i=0;i<vehiculos.length;i++){
    let Vehiculo = vehiculos[i];
    console.log('El vehiculo ' +Vehiculo._name+' es un/a ' +(Vehiculo.constructor.name)+ ' ,tiene ' +Vehiculo.capacidad()+ ' asientos, y tiene un volumen de ' +Vehiculo.volumen());
}