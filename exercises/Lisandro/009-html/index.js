const CELL_SIZE =64
const speed = 5;

const WIDTH=10
const HIGH=10
let w=0;
let h=0;
let pos_x = 0;
let pos_y = 0;
let cell_width = 64
let cell_high = 64

function setup() {
  // put setup code here
  w = WIDTH * CELL_SIZE;
  h = HIGH * CELL_SIZE;
  const canvas = createCanvas(w, h);
  canvas.parent('#canvasHolder');
}

function draw() {
    background(0);     
    fill('#2364AA');

    //rect(pos_x,pos_y,CELL_SIZE,CELL_SIZE);  
    //rect(w-CELL_SIZE,pos_y,CELL_SIZE,CELL_SIZE);  
    //rect((w-CELL_SIZE)/2,h-pos_y,CELL_SIZE,CELL_SIZE);  
    rect((w-CELL_SIZE)/2,(h-CELL_SIZE)/2,cell_width,cell_high);
    cell_width = cell_width+1;
      if(cell_width > (w+CELL_SIZE)/2){
        cell_width = 64;
      }
    cell_high = cell_high+1;
      if(cell_high > (h+CELL_SIZE)/2){
        cell_high = 64;
      }
    // pos_x = pos_x +  speed;
     pos_y = pos_y +  speed;
    // if(pos_x >= w )
    //    pos_x = 0;
    if(pos_y >= h )
      pos_y = 0; 
}

