
const fs = require('fs');
const path = require('path');

exports.listUsers = function (){
    let fullpath  =  path.join(__dirname,'data.json');
    let str = fs.readFileSync(fullpath,'utf-8');
    let data=  JSON.parse(str);
    return data.users;
}
exports.getUser = function (id){
    let fullpath  =  path.join(__dirname,'data.json');
    let str = fs.readFileSync(fullpath,'utf-8');
    let data=  JSON.parse(str);
    return data.users.find(p=> p.userId == id );
    // for(let i=0;i<data.users.length;i++){
    //     let user = data.users[i];
    //     if(user.userId == id)
    //        return user;
    // }
    // return null;
}
exports.addUser = function (user){
    let fullpath  =  path.join(__dirname,'data.json');
    let str = fs.readFileSync(fullpath,'utf-8');
    let data=  JSON.parse(str);

    user.userId = Math.max(data.users.map(p=> p.userId))+1;
    // let max;
    // for(let i=0;i<data.users.length;i++){
    //     let user = data.users[i]; 
    //     if(i==0){
    //         max = user.userId;
    //     }else{
    //         if(max < user.userId){
    //             max =user.userId; 
    //         }
    //     }  
    // }
    // user.userId = (max+1);
    data.users.push(user);
    str = JSON.stringify(data,null,2);
    fs.writeFileSync(fullpath,str,'utf8');

    return user;    
}
exports.updateUser = function (user){
    let fullpath  =  path.join(__dirname,'data.json');
    let str = fs.readFileSync(fullpath,'utf-8');
    let data=  JSON.parse(str);
    let updated=false;

    let currentUser = data.users.find(p=> p.userId == id );
    if(currentUser){
        currentUser.firstName = user.firstName;
            currentUser.age = user.age;
            currentUser.lastName = user.lastName;
            currentUser.phoneNumber = user.phoneNumber;
            currentUser.emailAddress = user.emailAddress;
            updated=true;
    }    
    // for(let i=0;i<data.users.length;i++){
    //     let currentUser = data.users[i];
    //     if(currentUser.userId == user.userId){
    //         currentUser.firstName = user.firstName;
    //         currentUser.age = user.age;
    //         currentUser.lastName = user.lastName;
    //         currentUser.phoneNumber = user.phoneNumber;
    //         currentUser.emailAddress = user.emailAddress;
    //         updated=true;
    //         break;
    //     }
    // }
    if(updated){   
        str = JSON.stringify(data,null,2);
        fs.writeFileSync(fullpath,str,'utf8');
    }
    return updated;    
}
exports.deleteUser = function (userId){
    let fullpath  =  path.join(__dirname,'data.json');
    let str = fs.readFileSync(fullpath,'utf-8');
    let data=  JSON.parse(str);

    let indexToDeleted;
    // for(let i=0;i<data.users.length;i++){
    //     let currentUser = data.users[i];
    //     if(currentUser.userId == userId){
    //         indexToDeleted = i;
    //         break;
    //     }
    // }
    indexToDeleted = data.users.findIndex(p=> p.userId == userId);
    if(indexToDeleted){
        data.users.splice( indexToDeleted, 1 );
        str = JSON.stringify(data,null,2);
        fs.writeFileSync(fullpath,str,'utf8');
        return true;
    }
    else{
        return false;
    }
}
