
// CRUD
// https://www.ionos.es/digitalguide/paginas-web/desarrollo-web/crud-las-principales-operaciones-de-bases-de-datos/


const fs = require('fs');
const path = require('path');

const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use("/", router);


function listUsers(){
    let fullpath  =  path.join(__dirname,'data.json');
    let str = fs.readFileSync(fullpath,'utf-8');
    let data=  JSON.parse(str);
    return data.users;
}
function getUser(id){
    let fullpath  =  path.join(__dirname,'data.json');
    let str = fs.readFileSync(fullpath,'utf-8');
    let data=  JSON.parse(str);
    for(let i=0;i<data.users.length;i++){
        let user = data.users[i];
        if(user.userId == id)
           return user;
    }
    return null;
}
function addUser(user){
    let fullpath  =  path.join(__dirname,'data.json');
    let str = fs.readFileSync(fullpath,'utf-8');
    let data=  JSON.parse(str);

    let max;
    for(let i=0;i<data.users.length;i++){
        let user = data.users[i]; 
        if(i==0){
            max = user.userId;
        }else{
            if(max < user.userId){
                max =user.userId; 
            }
        }  
    }
    user.userId = (max+1);
    data.users.push(user);
    str = JSON.stringify(data,null,2);
    fs.writeFileSync(fullpath,str,'utf8');

    return user;    
}

function updateUser(user){
    let fullpath  =  path.join(__dirname,'data.json');
    let str = fs.readFileSync(fullpath,'utf-8');
    let data=  JSON.parse(str);

    let updated=false;
    for(let i=0;i<data.users.length;i++){
        let currentUser = data.users[i];
        if(currentUser.userId == user.userId){
            currentUser.firstName = user.firstName;
            currentUser.age = user.age;
            currentUser.lastName = user.lastName;
            currentUser.phoneNumber = user.phoneNumber;
            currentUser.emailAddress = user.emailAddress;
            updated=true;
            break;
        }
    }
    if(updated){   
        str = JSON.stringify(data,null,2);
        fs.writeFileSync(fullpath,str,'utf8');
    }
    return updated;    
}
function deleteUser(userId){
    let fullpath  =  path.join(__dirname,'data.json');
    let str = fs.readFileSync(fullpath,'utf-8');
    let data=  JSON.parse(str);

    let deleted=false;
    //TODO:
}


app.get('/user', function (req, res) {
  let users = listUsers();  
  res.send(users);
});
app.get('/user/:id', function (req, res) {
    let user = getUser(req.params.id);  
    res.send(user);
}); 
app.post('/user', function (req, res) {     
    let user= addUser(req.body); 
    res.send(user);
});
app.put('/user', function (req, res) {     
    let updated= updateUser(req.body); 
    res.send(updated);
});
app.delete('/user/:id', function (req, res) {     
    let deleted= deleteUser(req.params.id); 
    res.send(deleted);
});

app.listen(3000);