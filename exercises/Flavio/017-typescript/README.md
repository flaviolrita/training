

# technologies
- Express 
- Pug 
- TypeScript

# commands
## build
```
npm run build
```
## dev
```
npm run dev
```
## prod
```
npm run start
```


# references
- [exercise](https://dev.to/aligoren/developing-an-express-application-using-typescript-3b1)