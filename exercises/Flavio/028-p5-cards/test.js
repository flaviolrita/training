
const numbers = ['A','2','3','4','5','6','7','8','9','10','J','K','Q'];
const figures = ['C','D','H','S'];


//Arma un mazo de blackjac el cual es la union de dos hasta 8 mazos
function buildblackjackDeck(cant=2){
    let blackjackDeck= [];
    for(let i =0;i<cant;i++){
        let deck = buildDeck();
        blackjackDeck = blackjackDeck.concat(deck)
    }
    return blackjackDeck;
}
  
  //armar un mazo
function buildDeck(){
    let cards =[];
    for(let i =0;i< figures.length;i++){   
        for(let j =0;j< numbers.length;j++){
        let card =numbers[j]+figures[i];
        cards.push(card);
        }
    }
    return cards; 
}

//hacer una funcion que mezcle mazos
function shuffle(array) {
    array.sort(() => Math.random() - 0.5);
}


//crear un mazo 
let blackjackDeck = buildblackjackDeck(2);
// muestra el mazo ordenado
console.log(blackjackDeck);
//mezcla
shuffle(blackjackDeck);
//muestra mazp mezclado
console.log(blackjackDeck);
//saca una carta
console.log(blackjackDeck.shift());
//muestra el mazo sin la carta que se extrajo
console.log(blackjackDeck);



class participant
{
    constructor(){
       this.cards = []; 
    }    
    assignCard(card){
        this.cards.push(card);
    }
    points(){
        let points = 0;
        for(let i =0;i< this.cards.length;i++){  
            let card = this.cards[i];
            let number = card.charAt(0);
            switch(number){
                case 'A':
                    points =+ 1;
                    break;
                case 'J': case 'K': case 'Q':
                    points =+ 10;
                    break;    
                default:
                    points =+ parseInt(number);     
            }
        }
        for(let i =0;i< this.cards.length;i++){
            let card = this.cards[i];
            let number = card.charAt(0);
            if(number=='A' && points<=11){
                points=+10;
                break;
            }
        }
        return points; 
    }
    
}

class dealer extends participant
{

}
class player extends participant
{
    constructor(tokens){
        this.tokens=tokens;
        this.bet=0;
        super();
    }
}

class play
{
    constructor(dealer,players){
        this.dealer = dealer;
        this.players = players;
        this.blackjackDeck = [];
    }

    init(){
        this.blackjackDeck = buildblackjackDeck(2);
        shuffle(this.blackjackDeck);
    }

    firstdDistribute(){
        
        //se reparte una carta a cada jugador
        for(let i =0;i< this.players.length;i++){
            let player = this.players[i];
            player.assignCard(this.blackjackDeck.shift());
        }
        //se reparte una carta al dealer
        this.dealer.assignCard(this.blackjackDeck.shift());
        //se reparte una segunda carta cada jugador
        for(let i =0;i< this.players.length;i++){
            let player = this.players[i];
            player.assignCard(this.blackjackDeck.shift());
        }        
    }
    
}