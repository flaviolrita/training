const grid = []
const WIDTH=150;
const HIGH=200;

const numbers = ['A','2','3','4','5','6','7','8','9','10','J','K','Q'];
const figures = ['C','D','H','S'];
const imgs = {};              

function preload() {
  for(let i =0;i< figures.length;i++){   
    for(let j =0;j< numbers.length;j++){
      let card =numbers[j]+figures[i];
      imgs[card] = loadImage('assets/'+card+'.png');
    }
  } 
}

function setup() {
  const w = numbers.length * WIDTH;
  const h = figures.length * HIGH;
  const canvas = createCanvas(w, h);
  canvas.parent('#canvasHolder');

  for(let i =0;i< figures.length;i++){ 
    let row = [];  
    for(let j =0;j< numbers.length;j++){
      let card =numbers[j]+figures[i];
      row.push(new Card(card,i,j));
    }
    grid.push(row);
  }

}

function draw() {
    background(0);
    for(let i =0;i< figures.length;i++){   
      for(let j =0;j< numbers.length;j++){
        grid[i][j].draw();
      }
    }
}




class Card{
  constructor(figure,x,y){
    this.x = x;
    this.y = y;
    this.figure = figure;
  }

  draw(){
     let i = this.x * HIGH;
    let j = this.y * WIDTH; 
    image(imgs[this.figure], j, i,WIDTH,HIGH);
  }   
}


