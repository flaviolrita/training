const fs = require('fs');
const path = require('path');

// lee el archivo data.json y agrega la propiedad fullName a los users.
function AddFullNameToFile(){

    let fullpath  =  path.join(__dirname,'data.json');
    let str = fs.readFileSync(fullpath,'utf-8');
    let data=  JSON.parse(str);

    for(let i=0;i<data.users.length;i++){
        let user = data.users[i];
        user.fullName = user.firstName+' '+user.lastName;
    }

    let resultPath  =  path.join(__dirname,'result.json');
    let result = JSON.stringify(data,null,2);
    fs.writeFileSync(resultPath,result,'utf8');
}

// lee un archivo de texto , vamos a crear un archivo de resultado con la cantidad de palablas por linea.

function wordsbyLine(){
    let fullpath  =  path.join(__dirname,'text.txt');
    let text = fs.readFileSync(fullpath,'utf-8');
    let lines = text.split('\r\n');
    let result ='start :'+(new Date().toLocaleString())+'\r\n';
    for(let i=0;i<lines.length;i++){
        let line = lines[i];
        let words = line.split(' ');
        result+='line '+(i+1)+' have '+words.length+'\r\n';
    }
    result+='end :'+(new Date().toLocaleString());
    let resultPath  =  path.join(__dirname,'result2.txt');
    fs.writeFileSync(resultPath,result,'utf8');
}

wordsbyLine();


// Exercises
// 1- leer el archivo data.json , filtrar los usuario cuyo nombre comience con "d" y guardarlo en otro archivo llamada resultExerc1.json
// 2- leer el archivo text.txt , crear un archivo llamado resultExerc2.txt solo con las parablas que comienzas con "e".
// 3- leer el archivo data.json y sumar todas las edades de los usuarios y crear un archivo que se llame  resultExerc3.txt  donde contenga la frase "La suma de todas las edades de usuarios es X" 










//console.log(data);

// var a = 2;
// var name = 'pedro';
// var numbers = [12,4,3];
// var obj = {name:'pedro',lastname:'perez'};

// console.log( typeof a);
// console.log( typeof name);
// console.log( typeof numbers);
// console.log( typeof obj);
// console.log( typeof data);
