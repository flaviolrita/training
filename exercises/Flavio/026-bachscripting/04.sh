#!/bin/bash
result=impar
if [ $1 -gt $2 ]; then
    result="$1 es mayor que $2"
elif [ $1 -lt $2 ]; then 
    result="$1 es menor que $2"
elif [ $1 -eq $2 ]; then 
    result="$1 es igual que $2"    
else 
   result="no se puede determinar la comparacion entre $1 y $2"     
fi
echo $result