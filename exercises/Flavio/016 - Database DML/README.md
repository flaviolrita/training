
# create tables
```
CREATE TABLE address (
  id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  street varchar(20)  NOT NULL DEFAULT '',
  number varchar(10) NOT NULL DEFAULT '',
  zipcode varchar(20) NOT NULL DEFAULT '',
  city varchar(80) NOT NULL DEFAULT '',
  state varchar(80) NOT NULL DEFAULT '',
  region varchar(80) NOT NULL DEFAULT '',
  country  varchar(80) NOT NULL DEFAULT '',
  PRIMARY KEY (id)
);

CREATE TABLE customer (
  id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  dni varchar(20)  NOT NULL DEFAULT '',
  name varchar(80) NOT NULL DEFAULT '',
  birthdate datetime NULL ,
  billingAdressId bigint(20) unsigned NULL,
  deliveryAddressId bigint(20) unsigned NULL,   
  PRIMARY KEY (id),
  FOREIGN KEY (billingAdressId) REFERENCES address(id)ON DELETE CASCADE,
  FOREIGN KEY (deliveryAddressId) REFERENCES address(id)ON DELETE CASCADE
);
```


# insert
## insert values
```
insert into address (street,number,zipcode,city,state,region,country) values ('callao', '10' ,'ASD232','CABA','BsAs','Central','Arg'); 
```
## inster from
```
insert into address (street,number,zipcode,city,state,region,country) 
select 'callao' street, '10' number,'ASD232' zipcode , 'CABA' city, 'BsAs' state, 'Central' region , 'Arg' country 
```

# select
```
SELECT city,street ,COUNT(1) 
FROM address a2  
WHERE CITY = 'CABA' OR  CITY = 'AVELLANEDA'
GROUP BY city,street
HAVING COUNT(1) >1
ORDER BY city,street DESC
LIMIT 20, 10
```
## count
```
select count(1)
from address
```

# update
```
update address set street = 'corrientes' 
where street = 'callao' and city = 'Avellaneda';
```

# delete 
```
delete from address 
where  number <10;
```

# store procedure

## create
```
create PROCEDURE myproc(IN count int)
BEGIN
    DECLARE i int DEFAULT 1;
    DECLARE _CITY VARCHAR(80);
    WHILE i <= count DO
    	
    	IF i mod 2 = 0 then
    		set _CITY = 'Avellaneda';
    	else
    	   set _CITY = 'CABA';
    	end if;
    
        insert into address (street,number,zipcode,city,state,region,country) 
		select 'callao' street, i number,'ASD232' zipcode , _CITY city, 'BsAs' state, 'Central' region , 'Arg' country; 
        set i = i + 1;
    END WHILE;
END;
```

## drop
```
drop PROCEDURE myproc;
```

## call
```
CALL myproc(50);
```