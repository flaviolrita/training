
# Commando Basicos

Agregar los cambios al stage
```
git add .
```

realiza un commit de los cambios
```
git commit -m "mensaje"
```

baja los cambios desde el repositorio remoto
```
git pull
```

sube los cambios del repositorio local al remoto
```
git push
```

# tutorials

- [tutorial](https://www.diegocmartin.com/tutorial-git/)
- [Comandos basicos](http://guides.beanstalkapp.com/version-control/common-git-commands.html)
- [Ejemplos de comandos](https://dzone.com/articles/top-20-git-commands-with-examples)  


- [gitbucket](https://www.atlassian.com/es/git/tutorials/learn-git-with-bitbucket-cloud)


# Git Flow

- [tutorial](https://www.atlassian.com/es/git/tutorials/comparing-workflows/gitflow-workflow)
- [tutorial](https://cleventy.com/que-es-git-flow-y-como-funciona/)