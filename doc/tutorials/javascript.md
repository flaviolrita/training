

# Iniciacion a Javascritp
- [tutorial basico](https://www.w3schools.com/js/js_variables.asp)
- [Referencia](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia)
- [tutorial](https://developer.mozilla.org/en-US/docs/Web/JavaScript)


- [Arrays methods](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array)
- [arrow expresssions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions)
- [lambda](https://www.vinta.com.br/blog/2015/javascript-lambda-and-arrow-functions/)


# libreria para graficos
- [p5](https://p5js.org/)

# Unit test
- [jest vs mocha](https://desarrolloactivo.com/blog/jest-vs-mocha/)
- [mocha](https://mochajs.org/)
- [mocha & chai](https://www.paradigmadigital.com/dev/testeando-javascript-mocha-chai/)
- [jest](https://jestjs.io/)
- [jest tutorial](https://jestjs.io/docs/es-ES/getting-started)
- [jest tutorial](https://tecnops.es/testing-en-javascript-con-jest-parte-1-de-2/)
- [mocks] (https://www.javiergarzas.com/2015/10/framework-mocks-mockito.html)

# Redis

- [package](https://www.npmjs.com/package/redis)
- [wiki](https://redis.js.org/)
- [tutorial](http://ualmtorres.github.io/howtos/RedisNodeJS/)
- [github](https://github.com/NodeRedis/node-redis)